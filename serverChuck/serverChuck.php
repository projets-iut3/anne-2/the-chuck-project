<?php 
/// Librairies éventuelles (pour la connexion à la BDD, etc.) 
	require("fact.php");
	 
	/// Paramétrage de l'entête HTTP (pour la réponse au Client) 
	header("Content-Type:application/json"); 
	 
	/// Identification du type de méthode HTTP envoyée par le client 
	$http_method = $_SERVER['REQUEST_METHOD']; 
	switch ($http_method){ 
	 
/// Cas de la méthode GET 
case "GET" : 
	/// Récupération des critères de recherche envoyés par le Client 
	if (empty($_GET['id'])){ 
		$facts = getAllFacts();
		echo $facts;
		deliver_response(200, "Azy tiens ...", $facts);
		break; 
	} else {
		$fact = getSingleFact($_GET['id']);
		echo $fact;
		deliver_response(200, "Azy tiens ...", $fact);
		break; 
	} 
	 
/// Cas de la méthode POST 
case "POST" : 

	/// Récupération des données envoyées par le Client 

	$postedData = file_get_contents('php://input'); 

	/// Traitement 
	json_decode($postedData, True);
	

	/// Envoi de la réponse au Client 

	//deliver_response(201, "Votre message", NULL); 

break; 
	 
/// Cas de la méthode PUT 
 
	case "PUT" : 
 
	/// Récupération des données envoyées par le Client 
 
	$postedData = file_get_contents('php://input'); 
 
	  
 
	/// Traitement 
 
 
	/// Envoi de la réponse au Client 
 
	deliver_response(200, "Votre message", NULL); 
 
	break; 
	 
/// Cas de la méthode DELETE 
 
	default : 
 
	 /// Récupération de l'identifiant de la ressource envoyé par le Client 
}